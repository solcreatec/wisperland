<?php

use App\Http\Controllers\ContactanosController;
use App\Http\Controllers\CursoController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // return view('welcome');
//     return "Bienvenido a la ruta principal";
// });

Route::get('/', HomeController::class)->name('home');

// Route::get('cursos', [CursoController::class, "index"])->name('cursos.index');

// Route::get('cursos/create', [CursoController::class, "create"])->name('cursos.create');

// Route::post('cursos/store', [CursoController::class, "store"])->name('cursos.store');

// Route::get('cursos/{curso}',[CursoController::class, "show"])->name('cursos.show');

// Route::get('cursos/{curso}/edit',[CursoController::class, "edit"])->name('cursos.edit');

// Route::put('cursos/{curso}',[CursoController::class, "update"])->name('cursos.update');

// Route::delete('cursos/{curso}',[CursoController::class, "destroy"])->name('cursos.destroy');

Route::resource('cursos', CursoController::class);

//view solo es para vistas estaticas sin paso de parametros
Route::view('nosotros', 'nosotros')->name('nosotros');
// Route::resource('asignaturas', CursoController::class)->parameters(['asignaturas'=>'curso'])->names('cursos');

Route::get('contactanos', [ContactanosController::class, 'index'])->name('contactanos.index');

Route::post('contactanos', [ContactanosController::class, 'store'])->name('contactanos.store');
// Route::get('cursos/{curso}/{categoria?}', function ($curso, $categoria=null) {

//     if ($categoria != null) {
        
//         return "Bienvenido al curso de $curso, del area de $categoria";
//     }else{
//         return "Bienvenido al curso de $curso";
//     }
    
// });