@extends('layouts.plantilla')

@section('title', 'Contactanos')

@section('content')

<h1>Dejanos un mensaje</h1>

<form action="{{route('contactanos.store')}}" method="POST">

    @csrf

    <label>Nombre:
        <input type="text" name="name" value="{{old('name')}}">
    </label>
 
    @error('name')
         <br>
         <small>*{{$message}}</small>
         <br>
    @enderror
    <br><br>
 
    <label>Correo electronico
        <input type="text" name="email" value="{{old('email')}}">
       </label>
    
        @error('email')
            <br>
            <small>*{{$message}}</small>
            <br>
        @enderror
       <br><br>

    <label>Mensaje:
    <br>
     <textarea name="mensaje" rows="4">{{old('mensaje')}}</textarea>
    </label>
 
    @error('mensaje')
         <br>
         <small>*{{$message}}</small>
         <br>
    @enderror
    <br><br>
    
    <button type="submit">Enviar</button>

</form>

@if (session('info'))
    <script>
        alert("{{session('info')}}");
    </script>
@endif
@endsection