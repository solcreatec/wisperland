@extends('layouts.plantilla')

@section('title', 'Editar curso')

@section('content')
<h1>Bienvenido a la pagina donde podrás editar un curso</h1>

<form action="{{route('cursos.update', $curso)}}" method="POST">

@csrf
@method('put')

   <label>Nombre
       <input type="text" name="name" value="{{old('name',$curso->name)}}">
   </label>

   @error('name')
        <br>
        <small>*{{$message}}</small>
        <br>
   @enderror
   <br><br>

   <label>Descripcion
    <textarea name="description" rows="5" >{{old('description', $curso->description)}}</textarea>
   </label>

   @error('description')
   <br>
   <small>*{{$message}}</small>
   <br>
   @enderror
   <br><br>

   <label>Categoria
    <input type="text" name="category" value="{{old('category', $curso->category)}}">
   </label>

   @error('category')
   <br>
   <small>*{{$message}}</small>
   <br>
   @enderror
   <br><br>

   <button type="submit">Actualizar</button>

</form>
@endsection    