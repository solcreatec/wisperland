@extends('layouts.plantilla')

@section('title', 'Crear curso')

@section('content')
<h1>Bienvenido a la pagina donde podrás crear un curso</h1>

<form action="{{route('cursos.store')}}" method="POST">

@csrf
{{-- @method('put') --}}

   <label>Nombre
       <input type="text" name="name" value="{{old('name')}}">
   </label>

   @error('name')
        <br>
        <small>*{{$message}}</small>
        <br>
   @enderror
   <br><br>

   <label>Descripcion
   <br>
    <textarea name="description" rows="5">{{old('description')}}</textarea>
   </label>

   @error('description')
        <br>
        <small>*{{$message}}</small>
        <br>
   @enderror
   <br><br>

   <label>Categoria
    <input type="text" name="category" value="{{old('category')}}">
   </label>

    @error('category')
        <br>
        <small>*{{$message}}</small>
        <br>
    @enderror
   <br><br>
   
   <button type="submit">Crear</button>

</form>
@endsection    