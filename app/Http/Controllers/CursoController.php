<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCurso;
use App\Models\Curso;
use Illuminate\Http\Request;
use Symfony\Contracts\Service\Attribute\Required;

class CursoController extends Controller
{
    //
    
    public function index()
    {
        // $cursos = Curso::all();
        $cursos = Curso::orderby('id', 'desc')->paginate();
        // return $cursos;
        return view('cursos.index', compact('cursos'));
    }

    public function create()
    {
        return view('cursos.create') ;
    }

    public function store(StoreCurso $request)
    {
        // $request->validate([
        //     'name'=> 'required|max:10',
        //     'description'=> 'required|min:10',
        //     'category'=> 'required',
        // ]);
        
        // $curso = new Curso();

        // $curso -> name = $request-> name;
        // $curso -> description = $request-> description;
        // $curso -> category = $request-> category;

        // $curso -> save();

        $curso = Curso::create($request->all());
        return redirect()->route('cursos.show', $curso);
        
        // return $curso;
        // $curso = Curso::find($id);
        // return view('cursos.show', ['curso'=>$curso]);
        // return view('cursos.show', compact('curso'));
    }

    public function show(Curso $curso)
    {
        // $curso = Curso::find($id);
        // return view('cursos.show', ['curso'=>$curso]);
        return view('cursos.show', compact('curso'));
    }

    public function edit(Curso $curso)
    {
        
        // $curso = new Curso();

        // $curso -> name = $request-> name;
        // $curso -> description = $request-> description;
        // $curso -> category = $request-> category;

        // $curso -> update();
        // return redirect()->route('cursos.show', $curso);
        // return $curso;
        // $curso = Curso::find(Curso);
        // return $curso;
        // return view('cursos.show', ['curso'=>$curso]);
        return view('cursos.edit', compact('curso'));
    }

    public function update(Request $request, Curso $curso)
    {
        
        $request->validate([
            'name'=> 'required',
            'description'=> 'required',
            'category'=> 'required',
        ]);

        // $curso -> name = $request-> name;
        // $curso -> description = $request-> description;
        // $curso -> category = $request-> category;
        // $curso -> save();
        $curso -> update($request->all());
        return redirect()->route('cursos.show', $curso);
        // return $curso.$request;
        // $curso = Curso::find(Curso);
        // return $curso;
        // return view('cursos.show', ['curso'=>$curso]);
        // return view('cursos.edit', compact('curso'));
    }

    public function destroy(Curso $curso)
    {

        $curso -> delete();
        return redirect()->route('cursos.index');

    }
}
